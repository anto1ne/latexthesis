# Template LaTeX - Thèse

Ce dépot propose un template de manuscripts de thèse.

Il comprend :

* une personnalisation de la mise en page (fichier /doc/look.tex)
* l'import de packages et l'ajout de commandes personnalisées pour l'écriture d'un document scientifique (fichier /doc/preambule.tex)
* une architecture par défaut du document (préface, corps du document, post-face)
* l'introduction automatique de listes de figures, tableaux, table des matières et références bibliographiques

## Compilation

Ce template se compile à l'aide de Pdflatex et Bibtex. Le fichier à compiler est main.tex

```bash
pdflatex main.tex
bibtex main.tex
pdflatex main.tex
pdflatex main.tex
```

## Compilation sous Texmaker

Afin de faciliter la compilation, texmaker peut être configurer de manière à réaliser toutes les étapes de compilation. La compilation se lance par la suite via l'option "Compilation rapide".

L'installation de Texmaker sous une distribution de type Gnu/Linux de base Debian peut se faire à l'aide 

`sudo apt install texmaker texlive-lang-french`


Le réglage de la compilation rapide sous Texmaker est le suivant 

![reglage](reglage.png)

